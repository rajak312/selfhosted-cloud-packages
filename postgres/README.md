# PostgreSQL Database Configuration

This folder contains the configuration files for the PostgreSQL database.

## Setup

The `init.sql` script sets up the initial database schema and roles.

## Usage

Run the SQL scripts within your PostgreSQL instance to create the schema and roles.

## Support

Contact database admin for more support.
