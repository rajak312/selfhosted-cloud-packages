#!/bin/bash

for item in *; do
    if [ -d "$item" ]; then
        tar -czf "${item}.tar.gz" "$item"
        echo "Created tar file for directory: $item"
    fi
done

