## to create a tar file

tar -czvf <generatedPath> <filePath>

```bash
    tar -czvf keycloak.tar.gz keycloak
```
