# Keycloak Configuration

This project contains the Keycloak configuration files.

## How to use

To use these configuration files, place them in the appropriate directory for your Keycloak instance and refer to them in your Keycloak server configuration.

## Contact

For any additional questions, please reach out to the admin.
